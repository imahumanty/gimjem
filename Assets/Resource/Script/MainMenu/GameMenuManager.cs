﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenuManager : MonoBehaviour
{
    public void PlayButton() {
        AudioManager.PlaySound("buttonClick");
        SceneManager.LoadScene("Main Game");
    }

    public void ButtonExit() {
        AudioManager.PlaySound("buttonClick");
        Application.Quit(0);
    }

    private void Update() {
        if(Input.GetKey(KeyCode.Escape)) {
            Application.Quit(0);
        }
    }
}
