﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ManagePeople")]
public class ManagementPeople : ScriptableObject
{
    public int peopleInHome = 0;
    public int peopleInFarm = 0;
    public int peopleQuarantine = 0;
}
